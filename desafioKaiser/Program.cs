﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace desafioKaiser
{
    class Program
    {
        static void Main(string[] args)
        {
            JogoDaVida Jogo = new JogoDaVida();
            int qtdeVivas;
            
            string mapa = "VVMMVVMVMVMVMVMMMVMMVVVVV";

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    Jogo.dados[x, y] = Convert.ToChar(mapa.Substring(x + y * 5, 1));
                }
            }

            for (int rodadas = 0; rodadas < 4; rodadas++)
            {
                Jogo = Jogo.Evoluir();
            }

            qtdeVivas = Jogo.QtdCelulasVivas();
            Console.WriteLine("Quantidade de células vivas: " + qtdeVivas);
            Console.WriteLine("Quantidade de células mortas: " + (5 * 5 - qtdeVivas));

            Console.ReadKey();
        }
    }
}
