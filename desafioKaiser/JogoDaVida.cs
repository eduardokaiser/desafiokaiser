﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace desafioKaiser
{
    public class JogoDaVida
    {
        public char[,] dados = new char[5, 5];
        private const char Viva = 'V';
        private const char Morta = 'M';

        public JogoDaVida Evoluir()
        {
            JogoDaVida novoJogo = new JogoDaVida();
            int qtdeVivas;

            novoJogo.dados = (char[,])dados.Clone();

            for (int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    qtdeVivas = QtdCelulasVivasVizinhas(x, y);
                    if (dados[x, y] == Viva)
                    {
                        // 1 teste = Qualquer célula viva com menos que duas células vivas vizinhas, morre, por baixa população
                        if (qtdeVivas < 2) dados[x, y] = Morta;
                        // 2 teste = Qualquer célula viva com mais que três células vivas vizinhas, morre, por alta população
                        if (qtdeVivas > 3) dados[x, y] = Morta;
                    }

                    if (dados[x, y] == Morta)
                    {
                        // 3 teste
                        if (qtdeVivas == 3) dados[x, y] = Viva;
                    }
                }
            }

            return novoJogo;
        }

        private int QtdCelulasVivasVizinhas(int x, int y)
        {
            int retorno;

            if (x < 0 || x > 4 || y < 0 || y > 4)
            {
                retorno = 0;
            }
            else
            {
                retorno = x > 0 ? dados[x - 1, y] == Viva ? 1 : 0 : 0;
                retorno += x < 4 ? dados[x + 1, y] == Viva ? 1 : 0 : 0;
                retorno += y > 0 ? dados[x, y - 1] == Viva ? 1 : 0 : 0;
                retorno += y < 4 ? dados[x, y + 1] == Viva ? 1 : 0 : 0;
            }
            return retorno;
        }

        public int QtdCelulasVivas()
        {
            int qtde = 0;

            for (int x = 0; x < 5; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    qtde += dados[x, y] == Viva ? 1 : 0;
                }
            }

            return qtde;
        }

    }

}
